﻿using System;
using System.Collections.Generic;

namespace ex_22
{
    class Program
    {
        static void Main(string[] args)
        {
            //-----------------------------------
            // EX #22 - Arrays and List<T>
            //-----------------------------------
            // Note: You only need a single repository for this task - everything can be coded in a single Program.cs
            // Note 2: When you go on to the next question, watch out for the names of the variables.
            // Note 3: If it helps with your program flow, write out a flow diagram to help you with your program

            int i;

            // a) Create an Array with 5 movie titles and display them each on a new line.
            Console.WriteLine("\n\n------------- A ---------------");

            var movies = new string[5]{"Lord of the Ring", "Matrix", "New World", "Pak&Save", "Daily"};
            for(i=0;i<movies.Length;i++)
            {
                Console.WriteLine($"Movie {i+1} : {movies[i]}");
            }

            // b) Change the title of the first movie and the title of the 3rd movie
            Console.WriteLine("\n\n------------- B ---------------");
            movies[0] = "Slave of the Ring";
            movies[2] = "Old World";

            // c) Sort the array of task b) in alphabetical order
            Console.WriteLine("\n\n------------- C ---------------");
            Array.Sort(movies);

            // d) Display on the screen the length of the array
            Console.WriteLine("\n\n------------- D ---------------");
            Console.WriteLine($"Length of the array movies : {movies.Length}");

            // e) Display on the screen the whole array as a string
            Console.WriteLine("\n\n------------- E ---------------");
            for(i=0;i<movies.Length;i++)
            {
                Console.WriteLine($"The title of movie {i+1} is '{movies[i]}'");
            }

            // f) Create a List<T> of 5 good places to eat here in Tauranga
            Console.WriteLine("\n\n------------- F ---------------");

            var restauruants = new List<string>();
            restauruants.Add("Takara");
            restauruants.Add("Chip and Fish");
            restauruants.Add("Macdolard");
            restauruants.Add("KFC");
            restauruants.Add("Oaktree");

            // g) Sort them in alphabetical order - display it on the screen
            Console.WriteLine("\n\n------------- G ---------------");
            Console.WriteLine("This is the top 5 of restaurants in Tauranga");
            restauruants.Sort();
            Console.WriteLine(string.Join(", ", restauruants));

            // h) Remove the 3rd place and display the remaining 4 on the screen
            Console.WriteLine("\n\n------------- H ---------------");
            restauruants.RemoveAt(2);   // Macdolard
            Console.WriteLine(string.Join(", ", restauruants));



        }
    }
}
